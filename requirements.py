import subprocess

def execute_python_file(file_path):
    subprocess.run(['python', file_path])

# Чтение списка библиотек из файла
with open('/storage/emulated/0/Documents/hollehe/Requests.txt') as f:
    requirements = f.read().splitlines()

# Установка библиотек
for requirement in requirements:
    subprocess.check_call(['pip', 'install', requirement])
    
execute_python_file("/storage/emulated/0/Documents/hollehe/pip.py")
def clear_console():
    os.system('cls' if os.name == 'nt' else 'clear')
    clear_console()